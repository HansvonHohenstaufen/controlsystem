## See LICENSE file for copyright and license details.
##
## SYSTEM ANALYSIS
##
## -- Function File: sysanalysis (G)
## -- Function File: sysanalysis (G, H)
## -- Function File: sysanalysis (G, H, DELTA)
## -- Function File: sysanalysis (G, H, DELTA, FILENAME)
## -- Function File: SDATA = sysanalysis (G)
## -- Function File: SDATA = sysanalysis (G, H, DELTA, FILENAME)
##
##	Get values of temporal parameters from LTI system analysis.
##
##	*Inputs*
##	G
##		LTI continuous model of forward transmission.
##	H
##		LTI continuous model of backward transmission. If not
#		specified, H=1.
##	DELTA
##		Criteria form setting time (ts), typically 5%,4% and 1%.
##		If not specified, delta=0.01 (1%).
##	FILENAME
##		Prefix from image's name to save in png format.
##
##	*Output*
##	SDATA
##		Struct with all system and response data.

function SDATA = sysanalysis(G, H=1, DELTA=0.01, FILENAME="")
	# System
	W	= feedback(G, H);
	GH	= G*H;

	# Poles and zeros
	printf("\nPoles and zeros");
	printf("\n---------------\n");
	#
	[p, z]	= pzmap(W);
	p
	z
	#
	SDATA.p	= p;
	SDATA.z	= z;

	# Check if system is stable
	printf("\nStable");
	printf("\n------\n");
	stable	= sum(real(p)>0);
	if stable > 0
		printf("NO\n");
	else
		printf("YES\n");
	endif

	# Root locus
	printf("\nRoot locus");
	printf("\n----------\n");
	#
	[rl, k0]	= rlocus(GH);
	k0
	#
	SDATA.klocus	= k0;

	# Transitory response only if system is stable
	if stable == 0
		# Impulse response
		printf("\nImpulse response");
		printf("\n----------------\n");
		SDATA.impulse = impulseresp(W, G, DELTA);

		# Step response
		printf("\nStep response");
		printf("\n-------------\n");
		SDATA.step = stepresp(W, G, DELTA);

		# EEs
		printf("\nStatic error");
		printf("\n------------\n");
		Kp		= getcee(GH, 0);
		SDATA.Kp	= Kp;

		Kv		= getcee(GH, 1);
		SDATA.kv	= Kv;

		Ka		= getcee(GH, 2);
		SDATA.ka	= Ka;
		Kp
		Kv
		Ka
	endif

	# Bode
	printf("\nBode");
	printf("\n----\n");
	#
	[Mg, Mp, wMg, wMp] = margin(GH);
	Mg = 20*log10(Mg)
	Mp
	#
	SDATA.bode.Mg	= Mg;
	SDATA.bode.Mp	= Mp;

	# Frequency response
	printf("\nFrequency response");
	printf("\n------------------\n");
	#
	SDATA.frequency	= frequency(W);

	# Save graphics
	savegraphics(W, GH, FILENAME);

	# End
	printf("\n")
	close all
endfunction

# Impulse transitory analysis
function SDATA = impulseresp(W, G, DELTA)
	# response
	[y, t, x]	= impulse(W);

	# cf
	cf		= y(size(y,1))
	SDATA.cf	= cf;

	# Mp
	[M tm]		= max(y);
	M		= (M - cf)/cf
	if 0.01 <= M
		tp		= t(tm)
		SDATA.tp	= tp;
	endif
	SDATA.M		= M;

	# ts
	ts = 0;
	tt = cf*DELTA;
	for i=1:size(x,1)
		if max(abs(y(i:size(x,1))-cf)) <= tt
			ts = t(i);
			break
		endif
	endfor
	ts
	SDATA.ts	= ts;
endfunction

# Step transitory analysis
function SDATA = stepresp(W, G, DELTA)
	# response
	[y, t, x]	= step(W);

	# cf
	cf		= y(size(y,1))
	SDATA.cf	= cf;

	# Mp
	[M tm]		= max(y);
	M		= (M - cf)/cf
	if 0.01 <= M
		tp		= t(tm)
		SDATA.tp	= tp;
	endif
	SDATA.M		= M;

	# tr
	for i=1:size(x,1)
		if y(i) <= 0.1*cf
			init = i;
		endif
		if y(i) >= 0.9*cf
			tr = t(i)-t(init);
			break;
		endif
	endfor
	tr
	SDATA.tr	= tr;

	# ts
	ts = 0;
	tt = cf*DELTA;
	for i=1:size(x,1)
		if max(abs(y(i:size(x,1))-cf)) <= tt
			ts = t(i);
			break
		endif
	endfor
	ts
	SDATA.ts	= ts;
endfunction

# Frequency analysis: close loop
function SDATA = frequency(W)
	# System
	[mag, pha, w] = bode(W);

	# M
	mdb	= 20*log10(mag);
	[m wm]	= max(mdb);
	wr	= w(wm);

	M	= m
	wr

	SDATA.M		= m;
	SDATA.wr	= wr;

	# BW
	init	= 0;
	bwt	= 0;
	BW	= 0;
	for i=1:size(mdb,1)
		if mdb(i) <= -3
			if bwt == 0
				init = w(i);
			else
				BW = w(i-1)-init;
				break;
			endif
		endif
		if mdb(i) >= -3
			bwt = 1;
		endif
	endfor
	BW
	SDATA.BW	= BW;
endfunction

function K = getcee(SYS, N)

	s = tf('s');
	G = SYS*(s^N);
	G = minreal(G);

	[tz tp tk tsam]	= zpkdata(G, "v");

	K	= real(tk*prod(tz)/prod(tp));
endfunction

# Generate and save graphics
function savegraphics(W, GH, FILENAME)
	# Check if save and generate graphics
	if size(FILENAME) == 0
		return
	endif
	# Poles and zeros
	pzmap(W, "*");
	print(strcat(FILENAME, "-pz.png"), "-S1000,1000");
	# Root locus
	rlocus(GH);
	print(strcat(FILENAME, "-rlocus.png"), "-S1000,1000");
	# Step response
	step(W)
	print(strcat(FILENAME, "-step.png"), "-S1000,1000");
	# Ramp response
	ramp(W)
	print(strcat(FILENAME, "-ramp.png"), "-S1000,1000");
	# Bode
	margin(GH)
	print(strcat(FILENAME, "-bode.png"), "-S1000,1000");
	# Frequency response
	bode(W)
	print(strcat(FILENAME, "-frequency.png"), "-S1000,1000");
endfunction
