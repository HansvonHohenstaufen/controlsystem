## See LICENSE file for copyright and license details.
##
## LAG COMPENSATOR DESIGN
##
## -- Function File: GC = lagcompensator(TYPE, GH, KE)
## -- Function File: GC = lagcompensator(TYPE, GH, KE, TYPE_ERROR)
## -- Function File: GC = lagcompensator(TYPE, GH, KE, MPP)
## -- Function File: GC = lagcompensator(TYPE, GH, KE, MPP, TYPE_ERROR)
##
##	Get lag compensator for one determinate system root.
##
##	*Input*
##	TYPE
##		Type from design: "t" for temporal, "f" for frequency.
##	GH
##		LTI model of open loop.
##	KS
##		System gain.
##	KE
##		Error constant.
##	TYPE_ERROR
##		Error constant type: static (0), velocity (1),
##		acceleration (2).
##	MPP
##		Phase margin.
##
##	*Output*
##	GC
##		LTI model of lag compensator.

function GC = lagcompensator(TYPE="t", GH, KE, TYPE_MPP=0, TYPE_ERROR=0)
	# Temporal
	if TYPE == "t"
		GC = temporal(GH, KE, TYPE_MPP);
	endif

	# Frequency
	if TYPE == "f"
		GC = frequency(GH, KE, TYPE_MPP, TYPE_ERROR);
	endif

	# End
	printf("\n")
endfunction

# Temporal design
function GC = temporal(GH, KE, TYPE_ERROR=0)
	# Package
	s = tf('s');

	# System
	GC = 1;
	Kc = 0;

	# Constants
	cbeta	= [8 10 12 15];
	cz	= [0.075 0.1 0.125 0.15];

	# Calculate error constanst K
	printf("\nCalculate error constanst");
	printf("\n-------------------------\n");
	#
	Gt		= minreal(GH*(s^TYPE_ERROR));
	[tz tp tk tsam]	= zpkdata(Gt, "v");
	nz		= sum(tz==0);
	np		= sum(tp==0);
	Kse		= 0;
	#
	#if np == nz
	if (np==0) && (nz==0)
		Kse	= tk*prod(tz)/prod(tp);
	elseif np > 0
		printf("Error constant is infinite for every value. \n")
		printf("Infinite solution.")
		return
	else
		printf("Error constant is zero for every value.\n")
		printf("Don't exist any solution.")
		return
	endif
	#
	Kse = abs(Kse)

	# Calculate beta
	printf("\nBeta");
	printf("\n----\n");
	#
	stages	= 0;
	beta	= 0;
	ibeta	= KE/Kse
	for i=1:5
		tlist = (ibeta^(1/i)) <= cbeta;
		if sum(tlist) > 0
			stages = i;
			beta =min(cbeta(tlist));
			break
		endif
	endfor
	#
	beta
	stages

	# Root
	printf("\nRoot");
	printf("\n----\n");
	#
	[num den]	= tfdata (GH, "v");
	tnum		= [zeros(1,size(den, 2)-size(num, 2)), num];
	rt		= roots(den+tnum);
	rt		= rt(imag(rt)>0)
	if length(rt)==0
		printf("Don't exist complex root\n")
		return
	endif

	# Zero and pole
	printf("\nZero and pole");
	printf("\n-------------\n");
	#
	minz	= -real(rt)/10
	zc	= max(cz(minz-cz >= 0));
	#
	if size(zc,2) == 0
		printf("Zero is not valid.\n")
		return
	endif
	#
	zc	= -zc
	pc	= zc/beta

	# Angle contribution < 5°
	printf("\nAngle contribution");
	printf("\n------------------\n");
	#
	ar	= (rt-pc)/(rt-zc);
	phid	= rad2deg(angle(ar))*stages
	#
	if 5 < phid
		printf("Angle contribution is major to 5°\n")
		return
	endif

	# Kc
	printf("\nCompensator gain");
	printf("\n----------------\n");

	Kc	= abs(ar)*beta

	# Compensator
	GC	= (zpk(zc,pc,Kc/beta))^stages;
endfunction

# Frequency design
function GC = frequency(G, KE, MPP, TYPE=0)
	# Package
	s = tf('s');

	# System
	GC = 0;

	# Constants
	cbeta	= [8 10 12 15];
	cz	= [0.075 0.1 0.125 0.15];

	# Calculate error constanst K
	printf("\nSystem's gain");
	printf("\n-------------\n");
	#
	Gt		= G*s^TYPE;
	[tz tp tk tsam]	= zpkdata(Gt, "v");
	nz		= sum(ismember(tz, 0));
	np		= sum(ismember(tp, 0));
	Kse		= 0;
	#
	if np == nz
		Kse	= tk*prod(tz(tz~=0))/prod(tp(tp~=0));
	elseif np < nz
		printf("Error constant is zero for every value.\n")
		if Ke == 0
			printf("Don't need a lag compensator.\n")
		else
			printf("Don't exist any solution.\n")
		endif
		return
	else
		printf("Error constant is infinite for every value.\n")
		printf("Don't exist any solution.\n")
		return
	endif
	#
	Kse = KE/abs(Kse)

	# Bode
	printf("\nMargins");
	printf("\n-------\n");
	[mag pha w]	= bode(Kse*G);
	[Mg Mp wMg WMp]	= margin(Kse*G);
	Mg
	Mp

	# New critique frequency
	printf("\nNew Wg");
	printf("\n------\n");

	Mpn	= -180+MPP;

	wg	= max(w(pha>=Mpn))
	mg	= min(mag(pha>=Mpn))

	# Calculate beta
	printf("\nBeta");
	printf("\n----\n");
	#
	stages	= 0;
	beta	= 0;
	ibeta	= mg
	for i=1:5
		tlist = (ibeta^(1/i)) <= cbeta;
		if sum(tlist) > 0
			stages = i;
			beta =min(cbeta(tlist));
			break
		endif
	endfor
	#
	beta
	stages

	# Zero and Pole
	printf("\nZero and Pole");
	printf("\n-------------\n");
	minz	= wg/10
	zc	= max(cz(minz-cz >= 0));
	#
	if size(zc,2) == 0
		printf("Zero is not valid.\n")
		return
	endif
	#
	z = -zc
	p = z/beta

	# Zero and Pole
	printf("\nCompensator's gain");
	printf("\n------------------\n");
	Kc = 1

	# Compensator;
	GC = ((s/z) + 1) / ((s/p)+1);
endfunction
