## See LICENSE file for copyright and license details.
##
## LEAD COMPENSATOR DESIGN
##
## -- Function File: [GC K] = leadcompensator(TYPE="t", RS, GH)
## -- Function File: [GC K] = leadcompensator(TYPE="f", GH, KE, MPP)
## -- Function File: [GC K] = leadcompensator(TYPE="f", GH, KE, MPP, TYPE_ERROR)
##
##
##	Get lead compensator for one determinate system root.
##
##	*Input*
##	TYPE
##		Type from design: "t" for temporal, "f" for frequency.
##	RS
##		Desired root.
##	GH
##		LTI continuous model of open loop.
##	KE
##		Error constant.
##	MPP
##		Phase margin in degree.
##	TYPE_ERROR
##		Error constant type: static (0), velocity (1),
##		acceleration (2).
##
##	*Output*
##	Gc
##		LTI model of lag compensator.
##	K
##		System gain.

function [GC K] = leadcompensator(TYPE="t", RS_GH, GH_KE, MPP=1, TYPE_ERROR=0)
	# Temporal
	if TYPE == "t"
		[GC K] = temporal(RS_GH, GH_KE);
	endif

	# Frequency
	if TYPE == "f"
		[GC K] = frequency(RS_GH, GH_KE, MPP, TYPE_ERROR);
	endif

	# End
	printf("\n");
endfunction

function [GC K] = temporal(RS, GH)
	# System
	[z p k tsam]	= zpkdata(GH, "v");

	# Contribution
	printf("\nContribution");
	printf("\n------------\n");
	#
	az = RS-z
	ap = RS-p

	# Deficit angle
	printf("\nDeficit angle");
	printf("\n-------------\n");
	#
	sysphi = 0;
	sysphi	= sum(angle(az)) - sum(angle(ap));
	sysphid	= rad2deg(sysphi)

	# Angle
	phi	= 0;
	# kmax = 2*k+1
	kmax	= 21;
	phi	= (-pi*kmax):(2*pi):(pi*kmax);
	phi	= phi-sysphi;
	phi	= min(phi(phi>=0));
	phid	= rad2deg(phi)

	# Stages
	printf("\nStages");
	printf("\n------\n");
	# max phi = 70
	maxphi	= 1.22;
	stages	= fix(phi/maxphi) + 1
	phi	= phi/stages
	phid	= rad2deg(phi)

	# Compensator's zero and pole
	printf("\nCompensator's zero and pole");
	printf("\n---------------------------\n");
	#
	thetaz	= pi/4 - phi/2 - 0.5*(angle(RS)-pi/2);
	thetap	= pi/4 + phi/2 - 0.5*(angle(RS)-pi/2);
	#
	zc	= -imag(RS)*tan(thetaz) + real(RS)
	pc	= -imag(RS)*tan(thetap) + real(RS)

	# System gain
	printf("\nSystem gain");
	printf("\n-----------\n");
	#
	K	= abs( prod(ap)/prod(az) )
	# Compensator gain
	printf("\nCompensator gain");
	printf("\n----------------\n");
	#
	Kc	= abs( (RS-pc)/(RS-zc) )

	# Compensator
	GC	= zpk(zc,pc,Kc)^stages;
endfunction

function [GC K] = frequency(GH, KE, MPP, TYPE=0)
	# Package
	s = tf('s');

	# System
	GC = 1;

	# Calculate error constanst K
	printf("\nSystem's gain");
	printf("\n-------------\n");
	#
	Gt		= minreal(GH*(s^TYPE));
	[tz tp tk tsam]	= zpkdata(Gt, "v");
	nz		= sum(tz==0);
	np		= sum(tp==0);
	Kse		= 0;
	#
	#if np == nz
	if (np==0) && (nz==0)
		Kse	= tk*prod(tz)/prod(tp);
	elseif np > 0
		printf("Error constant is infinite for every value. \n")
		printf("Infinite solution.")
		return
	else
		printf("Error constant is zero for every value.\n")
		printf("Don't exist any solution.")
		return
	endif

	K = KE/abs(Kse)

	# Bode
	printf("\nMargins");
	printf("\n-------\n");
	[mag pha w]	= bode(K*GH);
	[Mg Mp wMg WMp]	= margin(K*GH);
	Mg
	Mp

	# Deficit angle
	printf("\nDeficit angle");
	printf("\n-------------\n");
	phi	= MPP-Mp+12

	# alpha
	printf("\nAlpha");
	printf("\n-----\n");
	alpha	= (1-sind(phi))/(1+sind(phi))

	# New critique frequency
	printf("\nNew Wg");
	printf("\n------\n");
	aGH	= 1/sqrt(alpha);
	wg	= max(w(mag>=aGH))

	# Zero and Pole
	printf("\nZero and Pole");
	printf("\n-------------\n");
	z = wg*sqrt(alpha)
	p = z/alpha

	# Zero and Pole
	printf("\nCompensator's gain");
	printf("\n------------------\n");
	Kc = 1/alpha

	# Compensator;
	GC = ((s/z) + 1) / ((s/p)+1);
endfunction
