#!/usr/bin/env octave
pkg load control

s = tf('s');

# Leadcompensator: one
G = (s+3)/(s*(s^2+5*s+5));
r = -0.9+1.31i;
# Gc = 2.69*(s+1.47)/(s+1.7)
# K = 1.07
[Gc K] = leadcompensator("t", r, G)

# Leadcompensator: multiple
G = 10/(s*((s+9)^2));
H = 2;
r = -0.98+1.5i;
# Gc = 34.1*(s+0.88)^5/(s+3.63)^5
# K = 119.19
[Gc K] = leadcompensator("t", r, G, H)

# Leadcompensator: multiple
G = 10/(s*(s^2+4));
r = -2+2.62i;
# Gc = 8.64*(s+1.93)^4/(s+5.65)^4
# K = 34.86
[Gc K] = leadcompensator("t", r, G)
