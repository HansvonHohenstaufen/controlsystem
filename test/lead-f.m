#!/usr/bin/env octave
pkg load control

s = tf('s');

# One
G = 3/(s*(s+3));
H = (s+1)/(s+10);
[Gc K] = leadcompensator("f", G*H, 25, 28, 1)
# z = 24.26
# p = 34.66
# alpha = 0.7
# Kc = 1.43
