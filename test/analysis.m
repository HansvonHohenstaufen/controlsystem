#!/usr/bin/env octave

pkg load control

s = tf('s');

G = 35/(s*((s+5)^2));

sysanalysis(G, 1, 0.03);
