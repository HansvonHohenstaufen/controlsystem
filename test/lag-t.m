#!/usr/bin/env octave
pkg load control

s = tf('s');

# One stages
G = 22.62*5*(s+2)/(s*(s+10)*(s+6));
Kv = 10;
# beta = 8
# Gc = 8.08/beta*(s+0.15)/(s+0.01875)
Gc = lagcompensator("t", G, Kv, 1)

# Multiple stages
G = 150*5/(s*(s+8.5));
H = 1/(s+10);
Kv = 4000;
# beta = 8
# stages = 3
Gc = lagcompensator("t", G*H, Kv, 1)
