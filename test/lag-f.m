#!/usr/bin/env octave
pkg load control

s = tf('s');

# One stages
G = 3/(s*(s+3));
H = (s+1)/(s+10);
Kv = 25;
Mf = 28;
# beta = 8
# Gc = 1*(s/0.15+1)/(s/0.01875+1)
Gc = lagcompensator("f", G*H, Kv, Mf, 1);
